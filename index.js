const express = require('express');
const mongoose = require('mongoose');
const app = express();

mongoose.connect('mongodb://localhost/web-sink');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
    console.log('connected to db');

    var store = mongoose.Schema({
        name: 'string',
        time: 'date',
        data: 'object'
    });
    var Store = mongoose.model('Store', store);

    app.get("/", (req, res) => {
        Store.find().exec((err, stores) => {
            let ret_val = [];
            for (let s of stores) {
                ret_val.push({
                    name: s.name,
                    time: s.time,
                    data: s.data
                });
            }
            res.send(ret_val);
        });
    });

    app.get("/:store", (req, res) => {
        Store.create({
            name: req.params.store,
            data: req.query,
            time: Date.now()
        });
        res.send('Reeeeeeeee!');
    });

    app.listen(7337, () => {
        console.log('listening on 7337');
    });
});
