# Web Sink
A simple application for capturing query parameters. Requesting `/` will return
all captured values as JSON.

Example:
```
http://localhost:7337/test?hello=world
```
When visiting `http://localhost:7337/`:
```json
[
  {
    "name": "test",
    "time": "2018-06-15T23:13:49.721Z",
    "data": {
      "hello": "world"
    }
  }
]
```

## Installation
Run:
```
npm install
```

## Deployment
A great way to deploy NodeJS apps is with [pm2](https://pm2.io/).

1.  Clone this repository
2.  `cd web-sink`
3.  `pm2 start index.js --name web-sink`
4.  Set up an `nginx` reverse proxy to `http://127.0.0.1:7337` (I'll leave this
as an exercise for the reader)
